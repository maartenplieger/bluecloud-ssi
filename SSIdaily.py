# By Jan Willem Noteboom and Maarten Plieger - 2021-06-16

import netCDF4 as nc
import numpy as np
from netCDF4 import Dataset, num2date
import numba
from numba import jit

# I/O variables
inputfile = './C3S_ERA5_Medsea_1979_2020_allmonths_alldays.nc'
outputfile ='./SSI_Medsea_1979_2020_daily_new.nc'

dsInput = Dataset(inputfile)
try: dsInput.isopen()  #  make sure dataset is not alread open.
except: pass

# Get a refererence to the NetCDF windspeed and time variable
windspeedVariable=dsInput['wind']
timeVariable=dsInput['time']

# Keep the shape of the array in variables
nrhours = windspeedVariable.shape[0]
nrlats  = windspeedVariable.shape[1]
nrlongs = windspeedVariable.shape[2]

# Calculate the number of days from the shape
nrdays  = int(nrhours/24)

# Create the SSI array where we will store the results
SSI=np.zeros((nrdays, nrlats, nrlongs), dtype=np.float32)
days=np.zeros(nrdays, dtype=np.float32)

# Threshold for the SSI indicator
windspeed_threshold=15

# Iterate over every hour of the day and calculate the SSI index for each lat/lon grid cell, 
@jit(nopython=True)
def calcDay(SSI, windspeedPerDay, dayidx):
    for hourofdayidx in range(24):
        for latidx in range(nrlats):
            for longidx in range(nrlongs):
                SSI[dayidx][latidx][longidx] += 100 * max(0, windspeedPerDay[hourofdayidx,latidx, longidx] / windspeed_threshold - 1) ** 3

# Loop through each day, access the NetCDF data for that day and calculate the SSI index for that day.
for dayidx in range(nrdays):
    startHourIndex = (dayidx * 24)
    endHourIndex = startHourIndex+24
    days[dayidx]=timeVariable[startHourIndex]
    # Load the data from the NetCDF variable for this specific day (24 hours)
    windspeedPerDay = windspeedVariable[startHourIndex:endHourIndex][:][:]
    calcDay(SSI, windspeedPerDay, dayidx)
    print("Calculated day %d of %d" % (dayidx, nrdays))


#create outputfile
dsOutput = Dataset(outputfile,mode='w',format='NETCDF4')
dsOutput.title='SSI daily data'
dsOutput.subtitle="SSI daily data"

# copy global attributes all at once via dictionary
dsOutput.setncatts(dsInput.__dict__)
dsOutput.title='SSI daily data'
dsOutput.subtitle="SSI daily data"

# copy dimensions
lat_dim = dsOutput.createDimension('latitude', nrlats)     # latitude axis
lon_dim = dsOutput.createDimension('longitude', nrlongs)    # longitude axis
time_dim = dsOutput.createDimension('time', nrdays)   # time axis

toexclude = ['time', 'wind']
# copy all file data except for the excluded
for name, variable in dsInput.variables.items():
    if name not in toexclude:
        x = dsOutput.createVariable(name, variable.datatype, variable.dimensions)
        # copy variable attributes all at once via dictionary
        dsOutput[name].setncatts(dsInput[name].__dict__)
        dsOutput[name][:] = dsInput[name][:]

x = dsOutput.createVariable('time', np.int, ('time',))
dsOutput['time'].setncatts(dsInput['time'].__dict__)
dsOutput['time'][:] = days[:]

# Define SSI to hold the data
SSIday = dsOutput.createVariable('SSI',np.float32,('time','latitude','longitude')) # note: unlimited dimension is leftmost
SSIday.standard_name = 'SSI_day' # this is a CF standard name
SSIday[:,:,:] = SSI  # Appends data along unlimited dimension
print("-- Wrote data, SSI day shape is now ", SSIday.shape)

dsInput.close()
dsOutput.close()